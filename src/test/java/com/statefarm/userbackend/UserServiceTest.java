package com.statefarm.userbackend;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    private User user01;
    private User user02;
    private UpdateUserRequest updateUserRequest;
    private UserService userService;

    @Mock
    UserRepository userRepository;

    @BeforeEach
    void setUp() {
        user01 = new User("uname", "Test", "Dummy");

        user02 = new User("poppy", "Princess", "Poppy");
        user02.setPhone("123456789");
        user02.setEmail("Poppy@poppy.test");
        user02.setHub("Remote");
        user02.setCity("Musicland");
        user02.setState("US");
        user02.setProfilePicture(12345L);

        userService = new UserService(userRepository);
    }

    @Test
    void getAllUsersAndReturnAListOfUsers() {
        when(userRepository.findAll()).thenReturn(Arrays.asList(user01, user02));
        UsersList usersList = userService.getUsers();
        assertThat(usersList).isNotNull();
        assertThat(usersList.isEmpty()).isFalse();
    }

    @Test
    void getSpecificUserAndReturnsListWithFullAssert() {
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user02));

        User testUser = userService.getUser(user02.getUsername());

        assertThat(testUser).isNotNull();
        assertThat(testUser.getUsername()).isEqualTo(user02.getUsername());
        assertThat(testUser.getFirstName()).isEqualTo(user02.getFirstName());
        assertThat(testUser.getLastName()).isEqualTo(user02.getLastName());
        assertThat(testUser.getPhone()).isEqualTo(user02.getPhone());
        assertThat(testUser.getEmail()).isEqualTo(user02.getEmail());
        assertThat(testUser.getHub()).isEqualTo(user02.getHub());
        assertThat(testUser.getCity()).isEqualTo(user02.getCity());
        assertThat(testUser.getState()).isEqualTo(user02.getState());
        assertThat(testUser.getProfilePicture()).isEqualTo(user02.getProfilePicture());
    }

    @Test
    void getUserWithUsernameButDoesNotExistThusException() {
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());

        assertThatExceptionOfType(UserNotFoundException.class)
                .isThrownBy(() -> userService.getUser("THIS USER DOES NOT EXIST"));
    }

    @Test
    void patchUserWithHubCityStateAndReturnUser() {
        updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setFirstName("Bob");
        updateUserRequest.setLastName("BillyBob");
        updateUserRequest.setHub("Atlanta");
        updateUserRequest.setCity("Atlanta");
        updateUserRequest.setState("GA");
        updateUserRequest.setProfilePicture(43234L);
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user02));
        when(userRepository.save(any(User.class))).thenReturn(user02);

        User testUser = userService.updateUser(user01.getUsername(), updateUserRequest);

        assertThat(testUser).isNotNull();
        assertThat(testUser.getUsername()).isEqualTo(user02.getUsername());
        assertThat(testUser.getFirstName()).isEqualTo(user02.getFirstName());
        assertThat(testUser.getLastName()).isEqualTo(user02.getLastName());
        assertThat(testUser.getHub()).isEqualTo(user02.getHub());
        assertThat(testUser.getCity()).isEqualTo(user02.getCity());
        assertThat(testUser.getState()).isEqualTo(user02.getState());
        assertThat(testUser.getProfilePicture()).isEqualTo(user02.getProfilePicture());
    }

    @Test
    void patchUserButDoesNotFindUserToUpdateAndReturnNull() {
        updateUserRequest = new UpdateUserRequest();
        updateUserRequest.setHub("Atlanta");
        updateUserRequest.setCity("Atlanta");
        updateUserRequest.setState("GA");
        updateUserRequest.setProfilePicture(43234L);
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.ofNullable(any()));

        User testUser = userService.updateUser(user01.getUsername(), updateUserRequest);

        assertThat(testUser).isNull();
    }

    @Test
    void postValidUserAndReturnUser() {
        user01.setEmail("awesome@superhero.test");
        when(userRepository.save(any(User.class))).thenReturn(user01);

        User testUser = userService.addUser(user01);

        assertThat(testUser).isNotNull();
        assertThat(testUser.getUsername()).isEqualTo(user01.getUsername());
        assertThat(testUser.getFirstName()).isEqualTo(user01.getFirstName());
        assertThat(testUser.getLastName()).isEqualTo(user01.getLastName());
        assertThat(testUser.getEmail()).isEqualTo(user01.getEmail());
    }

    @Test
    void postUserWithUsernameButAlreadyExistThusException() {
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user01));

        assertThatExceptionOfType(UserAlreadyExistsException.class)
                .isThrownBy(() -> userService.addUser(user01));
    }

    @Test
    void deleteUserWithUsername() {
        user01.setEmail("awesome@superhero.test");
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(user01));

        userService.deleteUser(user01.getUsername());

        verify(userRepository).delete(any(User.class));
    }

    @Test
    void deleteUserWithUsernameButDoesNotExistThusException() {
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());

        assertThatExceptionOfType(UserNotFoundException.class)
                .isThrownBy(() -> userService.deleteUser("THIS USER DOES NOT EXIST"));
    }
}