package com.statefarm.userbackend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.statefarm.userbackend.utils.TokenTestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
public class UserBackEndControllerTests {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserService userService;

    ObjectMapper mapper = new ObjectMapper();

    TokenTestUtils tokenTestUtils = new TokenTestUtils("secret");

    String token;
    String adminToken;

    @BeforeEach
    void setUp() {
        List<String> roles = new ArrayList<>();
        roles.add("ROLE_USER");
        token = tokenTestUtils.getToken("AEIOU", roles);

        List<String> adminRole = new ArrayList<>();
        adminRole.add("ROLE_ADMIN");
        adminToken = tokenTestUtils.getToken("ABC12", adminRole);
    }

    @Test
    void user_DoesUserExist() {
        User user = new User("AEIOU", "Some", "Guy");
        assertThat(user).isNotNull();
    }

    @Test
    void user_DoesUserExist_onlyUserName() {
        User user = new User("AEIOU");
        assertThat(user).isNotNull();
    }

    @Test
    void GetUserNoParamsReturnUser() throws Exception {
        List<User> userList = new ArrayList<>();
        userList.add(new User("AEIOU"));
        when(userService.getUsers()).thenReturn(new UsersList(userList));
        mockMvc.perform(get("/api/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.userList", hasSize(1)));
    }

    @Test
    void getUserNoParamNoUserRtnNoContent() throws Exception {
        when(userService.getUsers()).thenReturn(new UsersList());
        mockMvc.perform(get("/api/users"))
                .andExpect(status().isNoContent());
    }

    @Test
    void addUserValidRtnUser() throws Exception {
        User user = new User("ABC123", "Other", "Guy");
        when(userService.addUser(any(User.class))).thenReturn(user);
        mockMvc.perform(post("/api/users").contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", token)
                        .content(mapper.writeValueAsString(user)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("username").value("ABC123"));
    }

    @Test
    void getUserByUserNameReturnUser() throws Exception {
        User user = new User("ABC123", "Other", "Guy");
        when(userService.getUser(anyString())).thenReturn(user);
        mockMvc.perform(get("/api/users/" + user.getUsername()).header("Authorization", token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("username").value(user.getUsername()));
    }

    @Test
    void updateUserWithObjectReturnUser() throws Exception {
        User user = new User("ABC123", "Other", "Guy");
        user.setPhone("1234567890");
        user.setEmail("guy@email.com");
        user.setCity("Springfield");
        when(userService.updateUser(anyString(), any())).thenReturn(user);
        mockMvc.perform(patch("/api/users/" + user.getUsername())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"phone\":\"1234567890\",\"email\":\"guy@email.com\",\"city\":\"Springfield\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("phone").value("1234567890"))
                .andExpect(jsonPath("email").value("guy@email.com"))
                .andExpect(jsonPath("city").value("Springfield"));
    }

    @Test
    void updateUserAndHaveAnNullUpdateResponseToTrigger204ResponseEntity() throws Exception {
        User user = new User("ABC123", "Other", "Guy");
        user.setPhone("1234567890");
        user.setEmail("guy@email.com");
        user.setCity("Springfield");
        when(userService.updateUser(anyString(), any())).thenReturn(null);
        mockMvc.perform(patch("/api/users/" + user.getUsername())
                        .header("Authorization", adminToken)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"phone\":\"1234567890\",\"email\":\"guy@email.com\",\"city\":\"Springfield\"}"))
                .andExpect(status().isNoContent());

        verify(userService).updateUser(anyString(), any());
    }

    @Test
    void deleteUserWithUserNameRtn202() throws Exception {
        mockMvc.perform(delete("/api/users/AEIOU")
                        .header("Authorization", adminToken))
                .andExpect(status().isAccepted());
        verify(userService).deleteUser(anyString());
    }

    @Test
        // DELETE: /api/users/{username} Returns 404, user not found
    void deleteUserThatDoesNotExistsAndReturns404() throws Exception {
        doThrow(new UserNotFoundException()).when(userService).deleteUser(anyString());
        mockMvc.perform(delete("/api/users/AABBC")
                        .header("Authorization", adminToken))
                .andExpect(status().isNotFound());
        verify(userService).deleteUser(anyString());
    }
}