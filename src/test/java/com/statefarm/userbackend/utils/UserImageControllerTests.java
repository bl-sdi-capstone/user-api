package com.statefarm.userbackend.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.statefarm.userbackend.UserImageController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(UserImageController.class)
public class UserImageControllerTests {

    @Autowired
    MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    TokenTestUtils tokenTestUtils = new TokenTestUtils("secret");

    String token;
    String adminToken;
}
