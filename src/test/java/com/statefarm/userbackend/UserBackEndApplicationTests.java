package com.statefarm.userbackend;

import com.statefarm.userbackend.utils.TokenTestUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserBackEndApplicationTests {
    @Autowired
    TestRestTemplate testRestTemplate;

    TokenTestUtils tokenTestUtils = new TokenTestUtils("secret");

    private String token;
    private String adminToken;
    private RestTemplate restTemplate;
    private User user01;
    private User user02;

    @BeforeEach
    void setup() {
        List<String> roles = new ArrayList<>();
        roles.add("ROLE_USER");
        token = tokenTestUtils.getToken("AEIOU", roles);

        List<String> adminRole = new ArrayList<>();
        adminRole.add("ROLE_ADMIN");
        adminToken = tokenTestUtils.getToken("ABC12", adminRole);

        user01 = new User("poppy", "Princess", "Poppy");
        user02 = new User("notfd", "Not", "Found");

        restTemplate = testRestTemplate.getRestTemplate();
        HttpClient httpClient = HttpClientBuilder.create().build();
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));
    }

    @AfterEach
    void teardown() {
    }

    private ResponseEntity<User> executePostForEntity(User testedUser) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", token);
        HttpEntity<User> request = new HttpEntity<>(testedUser, headers);

        String resourceURL = "/api/users";

        // Act
        return restTemplate.postForEntity(resourceURL, request, User.class);
    }

    private ResponseEntity<User> executePatchForEntity(User testedUser) throws JSONException {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", adminToken);

        JSONObject updateBody = new JSONObject();
        updateBody.put("hub", "Remote");
        updateBody.put("city", "My Basement");
        updateBody.put("state", "WA");
        String updateBodyString = updateBody.toString();

        HttpEntity<String> request = new HttpEntity<>(updateBodyString, headers);

        String resourceURL = "/api/users/" + testedUser.getUsername();

        // Act
        return restTemplate.exchange(resourceURL, HttpMethod.PATCH, request, User.class);
    }

    private ResponseEntity<User> executeDeleteForEntity(User testedUser) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", adminToken);
        HttpEntity<User> request = new HttpEntity<>(testedUser, headers);

        String resourceURL = "/api/users/" + testedUser.getUsername();

        // Act
        return restTemplate.exchange(resourceURL, HttpMethod.DELETE, request, User.class);
    }


    @Test
    void contextLoads() {
    }

    @Test
    void getExistingUsersWithNoneLoadedAndReturnsEmptyUserList() {
        // Act
        ResponseEntity<UsersList> getResponseEntity = restTemplate.getForEntity("/api/users", UsersList.class);
        // Assert
        assertThat(getResponseEntity).isNotNull();
        assertThat(getResponseEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(getResponseEntity.getBody()).isNull();
    }

    @Test
    void postUserAndVerifyReturnOfUserDetails() {
        // Arrange
        user01.setPhone("123456789");
        user01.setEmail("Poppy@poppy.test");
        user01.setHub("Remote");
        user01.setCity("Musicland");
        user01.setState("US");
        user01.setProfilePicture(12345L);

        ResponseEntity<User> postResponseEntity = executePostForEntity(user01);

        // Assert
        assertThat(postResponseEntity).isNotNull();
        assertThat(postResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        //// Ensure Content-Type is application/json
        assertThat(postResponseEntity.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        //// Ensure that POST updated Hub
        assertThat(Objects.requireNonNull(postResponseEntity.getBody()).getHub()).isEqualTo(user01.getHub());
    }

    @Test
    void postUserAndVerifyConflictResponse() {
        // Arrange
        user01.setPhone("123456789");
        user01.setEmail("Poppy@poppy.test");
        user01.setHub("Remote");
        user01.setCity("Musicland");
        user01.setState("US");
        user01.setProfilePicture(12345L);

        ResponseEntity<User> postResponseEntity = executePostForEntity(user01);

        // Assert
        assertThat(postResponseEntity).isNotNull();
        assertThat(postResponseEntity.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
        assertThat(postResponseEntity.getBody()).isNull();
    }

    @Test
    void getUserAndVerifyUserNotFound() {
        // Arrange
        String resourceURL = "/api/users/" + user02.getUsername();

        // Act
        ResponseEntity<User> getResponseEntity = restTemplate.getForEntity(resourceURL, User.class);

        // Assert
        assertThat(getResponseEntity).isNotNull();
        assertThat(getResponseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(getResponseEntity.getBody()).isNull();
    }

    @Test
    void getUserAndVerifyReturnOfUser() {
        // Arrange
        String currentUser = user01.getUsername();
        String resourceURL = "/api/users/" + currentUser;

        // Act
        ResponseEntity<User> getResponseEntity = restTemplate.getForEntity(resourceURL, User.class);

        // Assert
        assertThat(getResponseEntity).isNotNull();
        assertThat(getResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        //// Ensure Content-Type is application/json
        assertThat(getResponseEntity.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON);
        //// Ensure that GET is received
        assertThat(getResponseEntity.getBody()).isNotNull();
        assertThat(getResponseEntity.getBody().getUsername()).isEqualTo(currentUser);

    }

    @Test
    void patchUserAndVerifyNoContent() throws JSONException {
        // Act
        ResponseEntity<User> patchResponseEntity = executePatchForEntity(user02);

        // Assert
        assertThat(patchResponseEntity).isNotNull();
        assertThat(patchResponseEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(patchResponseEntity.getBody()).isNull();
    }

    @Test
    void patchUserAndVerifyReturnOfUserWasUpdated() throws JSONException {
        // Act
        ResponseEntity<User> patchResponseEntity = executePatchForEntity(user01);

        // Assert
        assertThat(patchResponseEntity).isNotNull();
        assertThat(patchResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(patchResponseEntity.getBody()).getHub()).isEqualTo("Remote");
        assertThat(Objects.requireNonNull(patchResponseEntity.getBody()).getCity()).isEqualTo("My Basement");
        assertThat(Objects.requireNonNull(patchResponseEntity.getBody()).getState()).isEqualTo("WA");
    }

    @Test
    void deleteUserVerifyNotFoundResponse() {
        String currentUser = user01.getUsername();
        String resourceURL = "/api/users/" + currentUser;

        // Act
        ResponseEntity<User> getResponseEntity = restTemplate.getForEntity(resourceURL, User.class);
        assertThat(getResponseEntity).isNotNull();
        assertThat(getResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(getResponseEntity.getBody()).isNotNull();

        // - intentional attempting to delete a non found user
        ResponseEntity<User> delResponseEntity = executeDeleteForEntity(user02);
        assertThat(delResponseEntity).isNotNull();
        assertThat(delResponseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(delResponseEntity.getBody()).isNull();
    }

    @Test
    void deleteUserVerifyAccepted() {
        String currentUser = user01.getUsername();
        String resourceURL = "/api/users/" + currentUser;

        // Act
        ResponseEntity<User> getResponseEntity = restTemplate.getForEntity(resourceURL, User.class);
        assertThat(getResponseEntity).isNotNull();
        assertThat(getResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(getResponseEntity.getBody()).isNotNull();
        assertThat(getResponseEntity.getBody().getUsername()).isEqualTo(currentUser);

        ResponseEntity<User> delResponseEntity = executeDeleteForEntity(user01);
        assertThat(delResponseEntity).isNotNull();
        assertThat(delResponseEntity.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
        assertThat(delResponseEntity.getBody()).isNull();
    }
}