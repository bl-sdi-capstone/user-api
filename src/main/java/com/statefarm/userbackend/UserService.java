package com.statefarm.userbackend;

import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UsersList getUsers() {
        return new UsersList(userRepository.findAll());
    }

    public User getUser(String username) {
        Optional<User> oUser = userRepository.findByUsername(username);
        if (oUser.isPresent()) {
            return oUser.get();
        } else {
            throw new UserNotFoundException();
        }
    }

    public User addUser(User user) {
        Optional<User> pcUser = userRepository.findByUsername(user.getUsername());
        if (pcUser.isPresent()) {
            throw new UserAlreadyExistsException();
        } else {
            return userRepository.save(user);
        }
    }

    public User updateUser(String username, UpdateUserRequest updateUserRequest) {
        Optional<User> oUser = userRepository.findByUsername(username);
        if (oUser.isPresent()) {
            if (updateUserRequest.getFirstName() != null) oUser.get().setFirstName(updateUserRequest.getFirstName());
            if (updateUserRequest.getLastName() != null) oUser.get().setLastName(updateUserRequest.getLastName());
            if (updateUserRequest.getPhone() != null) oUser.get().setPhone(updateUserRequest.getPhone());
            if (updateUserRequest.getEmail() != null) oUser.get().setEmail(updateUserRequest.getEmail());
            if (updateUserRequest.getHub() != null) oUser.get().setHub(updateUserRequest.getHub());
            if (updateUserRequest.getCity() != null) oUser.get().setCity(updateUserRequest.getCity());
            if (updateUserRequest.getState() != null) oUser.get().setState(updateUserRequest.getState());
            if (updateUserRequest.getProfilePicture() != null)
                oUser.get().setProfilePicture(updateUserRequest.getProfilePicture());
            return userRepository.save(oUser.get());
        }
        return null;
    }

    public void deleteUser(String username) {
        Optional<User> oUser = userRepository.findByUsername(username);
        if (oUser.isPresent()) {
            userRepository.delete(oUser.get());
        } else {
            throw new UserNotFoundException();
        }
    }
}