package com.statefarm.userbackend;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/api/users")
    public ResponseEntity<UsersList> getUsers() {
        UsersList usersList;
        usersList = userService.getUsers();
        return usersList.isEmpty() ? ResponseEntity.noContent().build() :
                ResponseEntity.ok(usersList);
    }

    @PostMapping("/api/users")
    public User addUser(@RequestBody User user) {
        return userService.addUser(user);
    }

    @GetMapping("/api/users/{username}")
    public User getUser(@PathVariable String username) {
        return userService.getUser(username);
    }

    @PatchMapping("/api/users/{username}")
    public ResponseEntity<User> updateUser(@PathVariable String username,
                           @RequestBody UpdateUserRequest update) {
        User user = userService.updateUser(username, update);
        return (user != null) ? ResponseEntity.ok(user) :
                ResponseEntity.noContent().build();
    }

    @DeleteMapping("/api/users/{username}")
    public ResponseEntity deleteUser(@PathVariable String username) {
        try {
            userService.deleteUser(username);
        } catch (UserNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.accepted().build();
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void InvalidUserExceptionHandler(UserNotFoundException e) {
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.CONFLICT)
    public void InvalidUserExceptionHandler(UserAlreadyExistsException e) {
    }
}