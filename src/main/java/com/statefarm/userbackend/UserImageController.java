package com.statefarm.userbackend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
public class UserImageController {

    @Autowired
    UserImageRepository userImageRepository;

    @Autowired
    UserRepository userRepository;

    @PostMapping("/api/users/{username}/image")
    User uploadImage(@RequestParam MultipartFile multipartImage, @PathVariable String username) throws Exception {
        Optional<User> user = userRepository.findByUsername(username);
        UserImage dbImage = new UserImage();
        dbImage.setName(multipartImage.getName());
        dbImage.setContent(multipartImage.getBytes());
        Long imageId = userImageRepository.save(dbImage).getId();
        if (user.isPresent()) {
            user.get().setProfilePicture(imageId);
            userRepository.save(user.get());
            return user.get();
        } else {
            throw new UserNotFoundException();
        }
    }

    @GetMapping(value = "/api/images/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    ByteArrayResource getImageByImageId(@PathVariable Long id) {
        byte[] image = userImageRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
                .getContent();
        return new ByteArrayResource(image);
    }

    @GetMapping(value = "/api/users/{username}/image", produces = MediaType.IMAGE_JPEG_VALUE)
    ByteArrayResource getImageByUsername(@PathVariable String username) {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isPresent()) {
            Long imageId = user.get().getProfilePicture();
            byte[] image = userImageRepository.findById(imageId)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
                    .getContent();
            return new ByteArrayResource(image);
        } else {
            throw new UserNotFoundException();
        }
    }
}
