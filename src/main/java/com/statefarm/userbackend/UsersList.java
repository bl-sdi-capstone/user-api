package com.statefarm.userbackend;

import java.util.ArrayList;
import java.util.List;

public class UsersList {

    private List<User> userList;

    public UsersList() {
        this.userList = new ArrayList<>();
    }

    public UsersList(List<User> users) {
        this.userList = users;
    }

    public List<User> getUserList(){
        return userList;
    }

    public boolean isEmpty() {
        return this.userList.isEmpty();
    }
}
